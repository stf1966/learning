# Learning (online, certificates, books, ...)

[My LinkedIn profil](http://www.linkedin.com/in/stephanegoubin)

## ToDo


## In progress


## Done

- [x] 2022-09 FreeCodeCamp Andrew Brown: [Hashicorp Terraform Associate Certification 13H00](https://www.youtube.com/watch?v=V4waklkBC38)
- [x] 2022-08 [Exporting to Netdata, Prometheus, Grafana stack](https://learn.netdata.cloud/docs/agent/exporting/walkthrough)
- [x] 2022-05 Xavki playlists: [xavki Tutoriels Terraform 34/34](https://www.youtube.com/playlist?list=PLn6POgpklwWrpWnv05paAdqbFbV6gApHx)
- [x] 2022-04 AWS Skill Builder: [introduction-to-aws-organizations](https://explore.skillbuilder.aws/learn/course/external/view/elearning/219/introduction-to-aws-organizations)
- [x] 2022-04 Udemy: [Terraform for the Absolute Beginners with Labs 4h30](https://www.udemy.com/course/terraform-for-the-absolute-beginners/)
- [x] 2022-03 Microsoft learn: [Explore the Windows Package Manager tool - 00H30](https://docs.microsoft.com/en-us/learn/modules/explore-windows-package-manager-tool/)
- [x] 2022-03 RedHat learn: [Ansible Basics - Automation Technical Overview (DO007) - 03H00](https://rhtapps.redhat.com/promo/course/do007)
- [x] 2022-01 Udemy: [GitLab CI: Pipelines, CI/CD and DevOps for Beginners - 05h00](https://www.udemy.com/certificate/UC-d1deb626-abc7-4f5a-8d1e-8e05dc5399c2/)
- [x] 2021-11 Udemy: [Building Automated Machine Images with HashiCorp Packer - 07h00](https://www.udemy.com/certificate/UC-2ad0b679-ae95-4025-8b12-097dd65fef7e/)
- [x] 2021-03 Udemy: [Kubernetes for the Absolute Beginners - 05h30](https://www.udemy.com/certificate/UC-fea84880-edce-43fb-8731-7f9dcddd7ad0/)

## Books

- [ ] 20xx-xx [Terraform - Up and Running: Writing Infrastructure As Code, 3rd Edition (isbn: 9781098116743)](https://www.google.com/search?q=isbn+9781098116743)
- [x] 2022-10 [Kubernetes: Up and Running, 3rd Edition (isbn: 9781098110208)](https://www.google.com/search?q=isbn+9781098110208)
- [x] 2022-10 [Ansible: Up and Running: Automating Configuration Management and Deployment the Easy Way](https://www.google.com/search?q=isbn+9781098109158) - [on github](https://github.com/ansiblebook)
- [x] 2022-04 [Unix and Linux system administration handbook (isbn: 9780134277554)](https://www.google.fr/search?q=isbn+9780134277554)
- [x] 2021-10 [Infrastructure-as-Code Automation Using Terraform, Packer, Vault, Nomad and Consul (isbn: 9781484271285)](https://www.google.fr/search?q=isbn+9781484271285)
- [x] 2021-07 [Ansible for DevOps (isbn: 9780986393426)](https://www.google.fr/search?q=isbn+9780986393426)
